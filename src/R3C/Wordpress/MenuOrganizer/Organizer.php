<?php
namespace R3C\Wordpress\MenuOrganizer;

class Organizer
{
	public function getUserMenus($id) {
		$option = get_option('organizador_menu' . '-' . $id, null);

		$option = unserialize($option);

		return json_encode($option);
	}
}
<?php

/**
 * BOOTSTRAP
 */
use R3C\Wordpress\Router\Router;

$router = new Router();
$router->addRoute('/api/menu/{id}/get-menus', 'R3C\Wordpress\MenuOrganizer\Organizer@getUserMenus');

$router->checkAndRun();
<?php

/**
 * @package Organizador do menu
 * @version 1.0
 */
/*
Plugin Name: Organizador de menu
Plugin URI: http://r3c.com.br
Description: Organiza os itens do menu lateral.
Author: R3C
Version: 1.0
Author URI: http://r3c.com.br
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once ABSPATH . '/../vendor/autoload.php';

$organizadorMenu = [];

add_action('admin_menu', 'organizador_add_menu');

function organizador_add_menu() {
    add_menu_page('Organizador do Menu', 'Organizador do Menu', 'manage_options', 'organizador_menu', 'organizador_criar', 'dashicons-editor-indent');
}

function organizador_criar() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	if (count($_POST)) {
		$_POST['menus'][] = 'organizador_menu';
		if ( ! get_option('organizador_menu' . '-' . $_POST['usuario'])) {

			$menus = serialize($_POST['menus']);
			add_option('organizador_menu' . '-' . $_POST['usuario'], $menus, '', 'yes');
		} else {
			$menus = serialize($_POST['menus']);
			update_option('organizador_menu' . '-' . $_POST['usuario'], $menus, '', 'yes');
		}
	}

	global $organizadorMenu;
	?>
	<h1 style="text-align: center;">Menus</h1>
	<p class="description" style="text-align: center;">Selecione abaixo os itens do menu que deseja excluir. Para selecionar múltiplos itens, utilize a tecla CONTROL do seu teclado.</p>

	<form method="POST">
		<div style="text-align: center; padding-bottom: 10px; padding-top: 20px;">
		<?php
			$users = get_users();
			$menus = get_option('organizador_menu' . '-' . $_POST['usuario'], null);
			$menus = unserialize($menus);
		?>

		<div style="padding-bottom: 10px;">
			<select name="usuario" id="usuario" style="width: 300px ">
				<option placeholder="Selecione o usuário"></option>
			<?php
				foreach ($users as $key => $user) :
					if ($user->caps['administrator'] && $user->data->ID != get_current_user_id()) : ?>
						<option value="<?php echo $user->data->ID; ?>"><?php echo $user->data->display_name; ?></option>
					<?php
					endif;
				endforeach;
			?>
			</select>
		</div>

			<select name="menus[]" id="menus" multiple style="height: 200px; width: 300px; ">
			<?php
			foreach ($organizadorMenu as $key => $menuItem) {
				if ( $menuItem[2] != 'index.php' && $menuItem[0] != '' && $menuItem[2] != 'organizador_menu') { ?>
					<option value="<?php echo $menuItem[2];?>"><?php echo strip_tags($menuItem[0]);?></option>
					<?php
				}
			}
			?>
			</select>
		</div>

		<div style="text-align: center;">
			<input type="submit" name="submit" class="button button-primary" value="Organizar">
		</div>
	</form>

	<input id="site-url" value="<?php echo str_replace('/wp', '', get_site_url()); ?>" type="hidden">

	<script>
		$ = jQuery;

		$('#usuario').change(function() {
			var url = $('#site-url').val() + '/api/menu/' + $(this).val() + '/get-menus';

	        $.get(url, {}, function(data) {
	        	$('#menus option').each(function(index, element) {
	        		$(element).html($(element).html().replace('[X]', ''));
	        		$(element).removeAttr('selected');
	        		$.each(data, function(key, value) {
	        			if (value == $(element).val()) {
	        				$(element).prepend('[X]');
	        				$(element).attr('selected', 'selected');
	        			}
	        		});
	        	});

	        }, 'json');
		});

	</script>
	<?php
}

add_action('admin_menu', 'atualiza_menus', 999);

function atualiza_menus() {
	global $menu;
	global $organizadorMenu;

	$organizadorMenu = $menu;
	$menus = get_option('organizador_menu' . '-' . get_current_user_id(), null);
	$menus = unserialize($menus);

	if ($menus) {
		foreach ($menus as $key => $item) {
			remove_menu_page($item);
		}
	}
}